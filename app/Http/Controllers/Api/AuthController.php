<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\SignupRequest;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Storage;
use Avatar;
use Illuminate\Auth\Events\Verified;
use App\Notifications\SignupActivate;

/**
 * @group Auth
 *
 * APIs for user authentication
 */
class AuthController extends Controller
{
    /**
     * Create user
     * Insert optional longer description of the API endpoint here.
     *
     * @bodyParam name string required User fullname. Example: Anzar Syahid
     * @bodyParam email string required Email. Example: zarchp@gmail.com
     * @bodyParam password string required Password. Example: 123qweasd
     * @bodyParam password_confirmation string required Password confirmation. Example: 123qweasd
     *
     * @response {
     *  "message": "Successfully created user"
     * }
     *
     * @return string message
     */
    public function signup(SignupRequest $request)
    {
        $data = $request->all();
        $data['password'] = bcrypt($data['password']);
        $data['activation_token'] = str_random(60);

        $user = User::create($data);
        $user->notify(new SignupActivate($user));

        $avatar = Avatar::create($user->name)->getImageObject()->encode('png');
        Storage::disk('public')->put('avatars/' . $user->id . '/avatar.png', (string) $avatar);

        return response()->json([
            'message' => 'Successfully created user'
        ], 201);
    }

    /**
     * User activation
     *
     * @queryParam token required description
     *
     * @return void
     */
    public function signupActivate()
    {
        $user = User::where('activation_token', request('activation_token'))->first();
        if (!$user) {
            return response()->json([
                'message' => 'This activation token is invalid'
            ], 404);
        }

        $user->email_verified_at = now();
        $user->is_active = true;
        $user->activation_token = '';
        $user->save();

        return response()->json([
            'message' => 'Successfully activated user'
        ], 404);
    }

    /**
     * Login
     *
     * @bodyParam email string required Email. Example: zarchp@gmail.com
     * @bodyParam password string required Password. Example: 123qweasd
     *
     * @response {
     *   "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjU0MzA5YzdhNmEzMjU5MGNlZjg2NzhkM2U0YmUzMDZmMTRjMGRmNzk1ZDU0MmY2YjYzNGYyMWYxZTkzN2I1MTkyMTE0NTExMWIzZGVmNzEyIn0.eyJhdWQiOiI2IiwianRpIjoiNTQzMDljN2E2YTMyNTkwY2VmODY3OGQzZTRiZTMwNmYxNGMwZGY3OTVkNTQyZjZiNjM0ZjIxZjFlOTM3YjUxOTIxMTQ1MTExYjNkZWY3MTIiLCJpYXQiOjE1NDE4MzgzODYsIm5iZiI6MTU0MTgzODM4NiwiZXhwIjoxNTczMzc0Mzg2LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.nZi8gShDVeSHNm2h0qQ-OGi41YENhQyFxRyBxx8hhjj77WhPmJ4qHvvtDPkSGwiGfOkpE5Fx6KYJFWrH5aIwBuLDFyKIrRUbqfGEFZfw4XoXLXsyAFZiE2NtzdmjiXYlrt5bpHDgeYlmqTjp80N0yI7XvMd6q0a60FBKurdfQgqws8Jp9g7Ol50WqSNWmQzoMnsm1l821Chlj_Qu-zp7z7Ck-RNy34kTiuPRKCLvThC9z5sDG0oWAqjjhJmfP0YJAUIQFu5Ml1YM4BhO1_K5DAKCKCGgIt0tXuFEXp5TxgvQ-ol6YcGTQRBve2KFGJz7FpuBoL1mOyx6GnCMtofUp1klIf1fjs87mCjZB_OOEkEELjOxPSJW5Bs9hEf5wt1VE3tpcJ1ZsQHiogi0IY2jl4EChlFds2aYSfIUmONDLOhDa2Nd8aJSgbXlOx0DFOG3XFixnKpZY6sCXp4cMmQXl9B6ZXhg_GBechiTtlOlCHXD9TZPwgxbiRCVbanudMTb2rDFkfXX1zojUTGF845mXf9KMU8hCgyEr9txOnEAbkVVQZjZ4RLhVagUlNqaMlCgb24wMsxCg0WKqQZzrpKA1DjXTJqJMutt-cvoipmZjdfB170g3YxCVVI_DmwTWiq6GrLyQ2ORNdgLOuGwKKaYw42EgpI6YGmND_DYo7lKSHY",
     *   "token_type": "Bearer",
     *   "expires_at": "2019-11-10 08:26:26"
     * }
     *
     * @return string access_token
     * @return string token_type
     * @return string expires_at
     */
    public function login(LoginRequest $request)
    {
        $credentials = request(['email', 'password']);
        $credentials['is_active'] = true;
        $credentials['deleted_at'] = null;

        $email = $request->email;
        $authField = filter_var($email, FILTER_VALIDATE_EMAIL) ? 'email' : 'mobile_phone';
        if ($authField == 'mobile_phone') {
            $email = filter_var($email, FILTER_SANITIZE_NUMBER_INT);
        }

        $user = User::query()
            ->where($authField, $email)
            ->where('password', Hash::check($request->password))
            ->first();
        if (!$user || !$user->hasVerifiedEmail()) {
            return response()->json([
                'message' => 'Unauthorized'
            ], 401);
        }

        $token = $user->createToken('Personal Access Token');

        return response()->json([
            'access_token' => $token->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => $token->token->expires_at->toDateTimeString()
        ], 200);
    }

    /**
     * Logout
     *
     * @authenticated
     *
     * @return string message
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();

        return response()->json([
            'message' => 'Successfully logged out'
        ], 200);
    }

    /**
     * Get the authenticated user
     *
     * @authenticated
     *
     * @return json user object
     */

    public function user(Request $request)
    {
        return response()->json([
            $request->user()
        ], 200);
    }
}
