<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Storage;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;
    use HasApiTokens;
    use SoftDeletes;

    protected $table = 'ytu_users';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name', 'email', 'password', 'is_active', 'activation_token', 'avatar'
    ];

    protected $hidden = [
        'password', 'remember_token', 'activation_token'
    ];

    protected $appends = ['avatar_url'];

    public function getAvatarUrlAttribute()
    {
        return url(Storage::url('avatars/' . $this->id . '/' . $this->avatar));
    }
}
