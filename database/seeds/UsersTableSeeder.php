<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ytu_users')->insert([
            'name' => 'Anzar Syahid',
            'email' => 'zarchp@gmail.com',
            'password' => bcrypt('123qweasd')
        ]);
    }
}
