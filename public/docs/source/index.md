---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://localhost/docs/collection.json)

<!-- END_INFO -->

#Auth

APIs for user authentication
<!-- START_9357c0a600c785fe4f708897facae8b8 -->
## Create user
Insert optional longer description of the API endpoint here.

> Example request:

```bash
curl -X POST "https://yaitu-api.dev/api/auth/signup" \
    -H "Content-Type: application/json" \
    -d "name"="Anzar Syahid" \
    -d "email"="zarchp@gmail.com" \
    -d "password"="123qweasd" \
    -d "password_confirmation"="123qweasd" 
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "https://yaitu-api.dev/api/auth/signup",
    "method": "POST",
    "data": {
        "name": "Anzar Syahid",
        "email": "zarchp@gmail.com",
        "password": "123qweasd",
        "password_confirmation": "123qweasd"
    },
    "headers": {
        "Content-Type": "application/json",
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "message": "Successfully created user"
}
```

### HTTP Request
`POST api/auth/signup`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    name | string |  required  | User fullname.
    email | string |  required  | Email.
    password | string |  required  | Password.
    password_confirmation | string |  required  | Password confirmation.

<!-- END_9357c0a600c785fe4f708897facae8b8 -->

<!-- START_8eff7ceae9ff5c9c6bcd2bdbafd79de6 -->
## User activation

> Example request:

```bash
curl -X GET -G "https://yaitu-api.dev/api/auth/signup/activate" \
    -H "Content-Type: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "https://yaitu-api.dev/api/auth/signup/activate",
    "method": "GET",
    "headers": {
        "Content-Type": "application/json",
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "message": "This activation token is invalid"
}
```

### HTTP Request
`GET api/auth/signup/activate`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    token |  required  | description

<!-- END_8eff7ceae9ff5c9c6bcd2bdbafd79de6 -->

<!-- START_a925a8d22b3615f12fca79456d286859 -->
## Login

> Example request:

```bash
curl -X POST "https://yaitu-api.dev/api/auth/login" \
    -H "Content-Type: application/json" \
    -d "email"="zarchp@gmail.com" \
    -d "password"="123qweasd" 
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "https://yaitu-api.dev/api/auth/login",
    "method": "POST",
    "data": {
        "email": "zarchp@gmail.com",
        "password": "123qweasd"
    },
    "headers": {
        "Content-Type": "application/json",
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjU0MzA5YzdhNmEzMjU5MGNlZjg2NzhkM2U0YmUzMDZmMTRjMGRmNzk1ZDU0MmY2YjYzNGYyMWYxZTkzN2I1MTkyMTE0NTExMWIzZGVmNzEyIn0.eyJhdWQiOiI2IiwianRpIjoiNTQzMDljN2E2YTMyNTkwY2VmODY3OGQzZTRiZTMwNmYxNGMwZGY3OTVkNTQyZjZiNjM0ZjIxZjFlOTM3YjUxOTIxMTQ1MTExYjNkZWY3MTIiLCJpYXQiOjE1NDE4MzgzODYsIm5iZiI6MTU0MTgzODM4NiwiZXhwIjoxNTczMzc0Mzg2LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.nZi8gShDVeSHNm2h0qQ-OGi41YENhQyFxRyBxx8hhjj77WhPmJ4qHvvtDPkSGwiGfOkpE5Fx6KYJFWrH5aIwBuLDFyKIrRUbqfGEFZfw4XoXLXsyAFZiE2NtzdmjiXYlrt5bpHDgeYlmqTjp80N0yI7XvMd6q0a60FBKurdfQgqws8Jp9g7Ol50WqSNWmQzoMnsm1l821Chlj_Qu-zp7z7Ck-RNy34kTiuPRKCLvThC9z5sDG0oWAqjjhJmfP0YJAUIQFu5Ml1YM4BhO1_K5DAKCKCGgIt0tXuFEXp5TxgvQ-ol6YcGTQRBve2KFGJz7FpuBoL1mOyx6GnCMtofUp1klIf1fjs87mCjZB_OOEkEELjOxPSJW5Bs9hEf5wt1VE3tpcJ1ZsQHiogi0IY2jl4EChlFds2aYSfIUmONDLOhDa2Nd8aJSgbXlOx0DFOG3XFixnKpZY6sCXp4cMmQXl9B6ZXhg_GBechiTtlOlCHXD9TZPwgxbiRCVbanudMTb2rDFkfXX1zojUTGF845mXf9KMU8hCgyEr9txOnEAbkVVQZjZ4RLhVagUlNqaMlCgb24wMsxCg0WKqQZzrpKA1DjXTJqJMutt-cvoipmZjdfB170g3YxCVVI_DmwTWiq6GrLyQ2ORNdgLOuGwKKaYw42EgpI6YGmND_DYo7lKSHY",
    "token_type": "Bearer",
    "expires_at": "2019-11-10 08:26:26"
}
```

### HTTP Request
`POST api/auth/login`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    email | string |  required  | Email.
    password | string |  required  | Password.

<!-- END_a925a8d22b3615f12fca79456d286859 -->

<!-- START_16928cb8fc6adf2d9bb675d62a2095c5 -->
## Logout

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET -G "https://yaitu-api.dev/api/auth/logout" \
    -H "Content-Type: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "https://yaitu-api.dev/api/auth/logout",
    "method": "GET",
    "headers": {
        "Content-Type": "application/json",
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/auth/logout`


<!-- END_16928cb8fc6adf2d9bb675d62a2095c5 -->

<!-- START_ff6d656b6d81a61adda963b8702034d2 -->
## Get the authenticated user

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET -G "https://yaitu-api.dev/api/auth/user" \
    -H "Content-Type: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "https://yaitu-api.dev/api/auth/user",
    "method": "GET",
    "headers": {
        "Content-Type": "application/json",
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/auth/user`


<!-- END_ff6d656b6d81a61adda963b8702034d2 -->

#general
<!-- START_8e9e2f7b6568d14b197402543cdaa874 -->
## Create token password reset

> Example request:

```bash
curl -X POST "https://yaitu-api.dev/api/password/create" \
    -H "Content-Type: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "https://yaitu-api.dev/api/password/create",
    "method": "POST",
    "headers": {
        "Content-Type": "application/json",
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/password/create`


<!-- END_8e9e2f7b6568d14b197402543cdaa874 -->

<!-- START_28b16f279c1333c2efecf8a89e31b59d -->
## Find token password reset

> Example request:

```bash
curl -X GET -G "https://yaitu-api.dev/api/password/find/{token}" \
    -H "Content-Type: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "https://yaitu-api.dev/api/password/find/{token}",
    "method": "GET",
    "headers": {
        "Content-Type": "application/json",
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "message": "This password reset token is invalid."
}
```

### HTTP Request
`GET api/password/find/{token}`


<!-- END_28b16f279c1333c2efecf8a89e31b59d -->

<!-- START_8ad860d24dc1cc6dac772d99135ad13e -->
## Reset password

> Example request:

```bash
curl -X POST "https://yaitu-api.dev/api/password/reset" \
    -H "Content-Type: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "https://yaitu-api.dev/api/password/reset",
    "method": "POST",
    "headers": {
        "Content-Type": "application/json",
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/password/reset`


<!-- END_8ad860d24dc1cc6dac772d99135ad13e -->


