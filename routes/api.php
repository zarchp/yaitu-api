<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('/user', function () {
    return 'gg';
})->middleware('auth:api');

Route::group([
    'prefix' => 'auth',
    'namespace' => 'Api',
    'as' => 'api.'
], function () {
    Route::post('signup', 'AuthController@signup')->name('auth.signup');
    Route::get('signup/activate', 'AuthController@signupActivate')->name('auth.signup.activate');
    Route::post('login', 'AuthController@login')->name('auth.login');

    Route::group([
        'middleware' => ['auth:api', 'verified']
    ], function () {
        Route::get('logout', 'AuthController@logout');
        Route::get('user', 'AuthController@user');
    });
});

Route::group([
    // 'namespace' => 'Auth',
    'middleware' => 'api',
    'prefix' => 'password'
], function () {
    Route::post('create', 'PasswordResetController@create');
    Route::get('find/{token}', 'PasswordResetController@find');
    Route::post('reset', 'PasswordResetController@reset');
});


Route::get('post', function () {
    return response()->json([
        [
            "id" => 1,
            "title" => "foo title",
            "body" => "bar body",
            "author" => 1
        ]
    ]);
});
Route::get('author', function () {
    return response()->json([
        [
            "id" => 1,
            "name" => "jhon",
            "email" => "jhon@mail.com"
        ]
     ]);
});

Route::get('hello', function () {
    return response()->json([
        "id" => 1,
        "name" => "Anzar Syahid",
        "email" => "zarchp@gmail.com"
    ]);
});
