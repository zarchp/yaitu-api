<?php

use Illuminate\Http\Request;
use App\Models\User;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// \Aschmelyun\Larametrics\Larametrics::routes();

Route::get('/', function () {
    // $user = User::find(1);
    // $token = $user->createToken('Token Name')->accessToken;

    return view('welcome');
});

Route::get('/redirect', function () {
    $query = http_build_query([
        'client_id' => '5',
        'redirect_uri' => 'https://yaitu-api.dev/callback',
        'response_type' => 'code',
        'scope' => 'public',
    ]);

    return redirect('https://yaitu-api.dev/oauth/authorize?'.$query);
});

Route::get('/callback', function (Request $request) {
    $http = new GuzzleHttp\Client;

    $response = $http->post('http://yaitu-api.dev/oauth/token', [
        'verify' => false,
        'form_params' => [
            'grant_type' => 'authorization_code',
            'client_id' => '5',
            'client_secret' => 'odoFPiRhWejmqX8ROkGT2jSbGYoHOil6HNMEQd4g',
            'redirect_uri' => 'https://yaitu-api.dev/callback',
            'code' => $request->code,
        ],
    ]);

    return json_decode((string) $response->getBody(), true);
});

Auth::routes(['verify' => true]);

Route::group(['middleware' => ['auth', 'verified']], function () {
    Route::get('/home', 'HomeController@index')->name('home');
});
